/*

*/
#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <Servo.h>
#include "esp32-hal-cpu.h"
#define SERVO_PIN 4

Servo servoAsc;


BLECharacteristic *pCharacteristic;
bool deviceConnected = false;
const int LED = LED_BUILTIN; // Could be different depending on the dev board. I used the DOIT ESP32 dev board.

std::string rxValue;

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/
#define SERVICE_UUID           "58cff59d-9276-413c-a9e9-1d40893aed7b" // UART service UUID
#define CHARACTERISTIC_UUID_READ "e526dd44-112f-446e-ad30-a68fe15ab4f2"

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      rxValue = pCharacteristic->getValue();

      if (rxValue.length() > 0) {
        // Serial.println("*********");
        // Serial.print("Received Value: ");

        // for (int i = 0; i < rxValue.length(); i++) {
        //   Serial.print(rxValue[i]);
        // }

        // Serial.println();

        // Do stuff based on the command received from the app
        if (rxValue.find("0") != -1) {
          // Serial.print("Etage 0!");
          // digitalWrite(LED, HIGH);
          servoAsc.write(0);
          // digitalWrite(LED, LOW);
        }
        else if (rxValue.find("1") != -1) {
          // Serial.print("Etage 1 !");
          // digitalWrite(LED, HIGH);
          servoAsc.write(180);
          // digitalWrite(LED, LOW);
        }

        // Serial.println();
        // Serial.println("*********");
      }
    }
};

void initBLE(){
  // Create the BLE Device
  BLEDevice::init("ASCRAF_XIX"); // Give it a name

  // Create the BLE Server
  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
                      CHARACTERISTIC_UUID_READ,
                      BLECharacteristic::PROPERTY_READ |
                      BLECharacteristic::PROPERTY_WRITE

                    );

  // pCharacteristic->addDescriptor(new BLE2902());

  pCharacteristic->setCallbacks(new MyCallbacks());

  // Start the service
  pCharacteristic->setValue("Hello World");
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");
}

void setup() {
  Serial.begin(115200);
  // reduce the battery consumption
  // https://savjee.be/2019/12/esp32-tips-to-increase-battery-life/
  setCpuFrequencyMhz(80);

  initBLE();
  servoAsc.attach(SERVO_PIN);
  pinMode(LED, OUTPUT);
  pinMode(SERVO_PIN,OUTPUT);

}

void loop() {
  if (deviceConnected) {
  //  if (rxValue.find("A") != -1) {
  //    Serial.println("Turning ON!");
  //    digitalWrite(LED, HIGH);
  //  }
//    else if (rxValue.find("B") != -1) {
//      Serial.println("Turning OFF!");
//      digitalWrite(LED, LOW);
//    }
    // Serial.print("rxValue");
    // Serial.print(rxValue);
  }
  delay(1000);
}